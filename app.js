/**
	@flow
**/
import express from 'express';
import path from 'path';
import favicon from 'serve-favicon';
import logger from 'morgan';
import bodyParser from 'body-parser';
import expressLayouts from 'express-ejs-layouts';
import * as RoomControl from './utils/RoomControl';
import * as TimeControl from './utils/TimeControl';
import _ from 'lodash';
import { io } from './utils/RoomControl';
import { roomSetting } from './config';

import {
	room,
	race,
	result,
} from './routes/index';

var app = express();

// Socket.io
app.io = io;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('layout', 'layout');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(expressLayouts);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', room(io));
app.use('/race', race(io));
app.use('/result', result);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err: Error = new Error('Not Found');
  //err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
	res.status(err.status || 500);
	res.render('error', {
	  message: err.message,
	  error: err
	});
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
	message: err.message,
	error: {}
  });
});

// socket.io events
io.on( "connection", function( socket )
{
	//Room Events
	//建立遊戲房間
	socket.on('/createRoom', function(data){
		RoomControl.getNewRoom(socket.id).then((roomInfo) => {
			roomInfo.max = roomSetting.max;
			socket.emit('/newRoom', roomInfo);
			socket.join(`${roomInfo.id}`);
		});
	});

	//user申請加入遊戲
	socket.on('/invited', (data) => {

		let user = Object.assign(data, {
			id : socket.id,
			progress: 0,
			connect: true,
			spend: 0,
			complete: false,
		});
		const rs = RoomControl.addUser(user);

		if(rs.check){
			const room = RoomControl.getRoom(user.roomID);
			socket.to(`${user.roomID}`).emit('/serverAddNewUser', {
				invited: room.users.length,
				caninvited: roomSetting.max - room.users.length
			});

			socket.to(`${data.roomID}`).emit('/clientAddNewUser', {
				user: user,
				nickname: user.nickname
			});

			//client 加入遊戲成功
			socket.emit('/addRoomSuccess', {
				roomID: user.roomID,
				goalCount: roomSetting.goalCount
			});

			socket.join(`${user.roomID}`);
		}else{
			io.emit('/Error', {
				mode: "addError",
				message: rs.message
			});
		}
	});

	//遊戲開始
	socket.on("/startGame", (data) => {
		let room = RoomControl.getRoom(data.roomID);
		if(room.users.length < 1){
			socket.emit('/serverError', {
				message : "沒有玩家無法開始"
			});
		}else{
			room = RoomControl.updateRoom(room.id, {
				startTime: TimeControl.add(10, 'seconds'),
				status : 'game'
			});

			io.emit('/startGame', {
				roomID: room.id,
				users: room.users,
				progress: 0,
				goal: roomSetting.goalCount,
				startTime: room.startTime
			});
		}
	});

	//馬兒向前跑
	socket.on("/run", (data) => {
		const user = RoomControl.userRun(data.roomID, socket.id);
		let newstatus = _.clone(data);
		newstatus.user  = user;
		io.emit('/newStatus', newstatus);
	});

	//斷線
	socket.on('disconnect', () => {
		const user = RoomControl.disconnect(socket.id);
		let room = RoomControl.getRoom(user.roomID);
		if(user !== false){
			if(room.status === 'ready'){
				RoomControl.leaveRoom(user.roomID, user.id);
				socket.to(`${user.roomID}`).emit('/serverAddNewUser', {
					invited: room.users.length,
					caninvited: roomSetting.max - room.users.length
				});
			}else{
				RoomControl.disableUser(user.roomID, user.id);
				user.connect = false;
			}
			io.emit('/leaveRoom', user);
		}
	});
});

export default app;
