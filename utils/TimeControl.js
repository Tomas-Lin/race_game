/**
	@flow
**/

import moment from 'moment';

export function getCountDown(start, end){
	let diffTime = end.format('x') - start.format('x');
	return moment.duration(diffTime, 'milliseconds');

}
export function getMoment(): moment{
	return moment;
}

export function now(): moment{
	return moment();
}

export function add(num: number, unit:string):moment{
	return moment().add(num, unit);
}
