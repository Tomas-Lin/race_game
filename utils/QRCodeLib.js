/**
	@flow
**/
import QRCode from 'qrcode';

export function toDataURL(source: string): Promise{
	return new Promise((resolve, reject) => {
		QRCode.toDataURL(source, (err, url) => {
			if(err) reject(err);
			else resolve(url);
		});
	});
}
