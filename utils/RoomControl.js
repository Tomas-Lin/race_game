/**
	@flow
**/
import crypto from 'crypto';
import _ from 'lodash';
import * as Time from './TimeControl';
import { toDataURL } from './QRCodeLib';
import { ServerURL, roomSetting, ClientURL } from '../config';

let socket_io    = require( "socket.io" );

// Socket Object
export let io   = socket_io();

let roomsgroup = [];

export function getNewRoom(socketID:string){
	var buf = crypto.randomBytes(32);
	const roomID = buf.toString('base64').replace(/[+|=|\/]/g, '');
	return toDataURL(`${ServerURL}/client/game.html?roomID=${roomID}`).then((qrcode) => {
		let roomInfo = {
			id : roomID,
			socketID: socketID,
			place: 0,
			status: 'ready',
			qrcode: qrcode,
			complete : false,
			startTime: false,
			endTime: false,
			goalCount: roomSetting.goalCount,
			users: [],
		};
		roomsgroup.push(roomInfo);
		return roomInfo;
	});
}

export function addUser(user){
	let room = _.find(roomsgroup, (room) => {
		return user.roomID === room.id;
	});
	if(_.isUndefined(room)){
		return {
			check : false,
			message: 'roomID錯誤'
		};
	}else if(room.users.length >= roomSetting.max){
		return {
			check : false,
			message: '已額滿'
		};;
	}else{
		room.users.push(user);
		return {
			check: true
		};
	}
}

export function getSocketID(): string{
	return io.socket.id;
}

export function disableUser(roomID: string, uid: string){
	const room = _.find(roomsgroup, (r) => {
		return r.id === roomID;
	});
	let users = _.filter(room.users, (user) => {
		return user.id !== uid;
	});
	room.users = users;
	return room;
}

export function leaveRoom(roomID: string, uid: string){
	const room = _.find(roomsgroup, (r) => {
		return r.id === roomID;
	});
	let users = _.filter(room.users, (user) => {
		return user.id !== uid;
	});
	room.users = users;
	return room;
}

export function getAllRooms(status):Array<object>{
	return _.filter(roomsgroup, (room) => {
		return room.status === status;
	});
}

export function getRoom(roomID:string): Object{
	return _.find(roomsgroup, (room) => {
		return room.id === roomID;
	});
}

export function updateRoom(roomID: string, options= {}){
	let room = getRoom(roomID);
	for(const key in options){
		const value = options[key];
		room[key] = value;
	}
	return room;
}

export function userRun(roomID: string, uid: string){
	let room = getRoom(roomID);
	let user = _.find(room.users, (u) => {
		return u.id === uid;
	});
	if(user.complete){
		return user;
	}else{
		user.progress++;
		if(user.progress >= roomSetting.goalCount){
			let place = _.filter(room.users, (u) => {
				return u.place !== 0;
			});
			user.place = place.length;
			user.complete = true;
			user.endTime = Time.now();
		}

		return user;
	}
}

export function disconnect(socketID){
	let user;
	const room_rs = _.find(roomsgroup, (room) => {
		return socketID === room.socketID;
	});
	if(_.isUndefined(room_rs)){
		roomsgroup.map((room) => {
			if(_.isUndefined(user)){
				user = _.find(room.users, (user) => {
					return user.id === socketID;
				});
			}
		});
		if(user) return user;
		else return false;
	}else{
		return false;
	}
}

export function removeRoom(): void{

}
