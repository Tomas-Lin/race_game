/**
	@flow
**/

export const ServerURL: string = 'http://tomas.rain-maker.com.tw';

export const ClientURL: string= 'http://45.79.143.70';

export const roomSetting: {
	max: number;
	goalCount: number;
} = {
	max : 10,
	goalCount: 10,
};

export const horseColor: Array<string> = ['black'];

export const horsePng: Array<string> = ['horse.png'];
