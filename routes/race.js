/**
	@flow
**/
import express from 'express';
const { Router } = express;

export default function(io): Router{
	var router = Router();

	/* GET race page. */
	router.get('/', (req, res, next) => {
	  res.render('race', { title: 'Room' });
	});

	router.get('/client/:roomID', (req, res, next) => {
		const { roomID } = req.params;

		res.render('race_client', { title: 'RaceClient' });
	});

	io.on('connection', (socket) => {
	});

	return router;
}
