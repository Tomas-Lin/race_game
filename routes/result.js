/**
	@flow
**/
import express from 'express';
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('result', { title: 'Result' });
});

router.get('/client', function(req, res, next) {
  res.render('result_client', { title: 'Result Client' });
});

export default router;
