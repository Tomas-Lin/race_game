/**
	@flow
**/
import express from 'express';
import { toDataURL } from  '../utils/QRCodeLib';
import _ from 'lodash';
import { getNewRoom, getRoom } from '../utils/RoomControl';
import { getCountDown } from '../utils/TimeControl';
import { ServerURL, roomSetting } from '../config';
const { Router } = express;

export default function(io): Router {

	var router = express.Router();

	/* GET room page. */
	router.get('/', function(req, res, next) {
		const newRoom = getNewRoom();
		newRoom.then((room) => {
			res.render('home', {
			  title: 'Home',
			  roomID: room.id,
		  });
		});

	});

	/* GET room page. */
	router.get('/room/:id', function(req, res, next) {
		const room = getRoom(req.params.id);
		if(_.isUndefined(room)){
			res.redirect('/');
		}else{
			const countdown = getCountDown(room.readyStart, room.readyEnd);
			res.render('index', {
			  title: 'Room',
			  roomID: room.id,
			  clientURL: room.qrcode,
			  countDown: countdown,
			  roomSetting : roomSetting,
			  invited: room.users.length
		  });
		}

	});

	/* GET invited page. */
	router.get('/invited/:roomID', function(req, res, next) {
		const { roomID } = req.params;
	  res.render('invited', {
		  title: 'Invited',
		  roomID: roomID
	   });
	});

	return router;

}
